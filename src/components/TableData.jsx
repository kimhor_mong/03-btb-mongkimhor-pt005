import React from 'react';
import { Container, Table, Button, Badge } from 'react-bootstrap';

function TableData(props) {
	let items = props.items;
	return (
		<Container>
			<div className='my-4'>
				<Button onClick={props.onReset} variant='info'>
					Reset
				</Button>
				<Badge className='ml-2' variant='warning'>
					{props.items.length} count
				</Badge>
			</div>
			<Table>
				<thead>
					<tr>
						<th>#</th>
						<th>Food</th>
						<th>Amount</th>
						<th>Price</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					{items.map((item) => (
						<tr key={item.id}>
							<td>{item.id}</td>
							<td>{item.title}</td>
							<td>{item.amount}</td>
							<td>{item.price}</td>
							<td>{item.amount * item.price}</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
}

export default TableData;
